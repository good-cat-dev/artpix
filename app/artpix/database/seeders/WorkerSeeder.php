<?php

namespace Database\Seeders;

use App\Models\Worker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WorkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $workers = [
            'Andrey', 'Sergey', 'Mikhail', 'Stas', 'Artem',
            'Tatiana', 'Eugene', 'Katya', 'Boris',
        ];

        foreach ($workers as $workerName) {
            Worker::create(['name' => $workerName]);
        }
    }
}
