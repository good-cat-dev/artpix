<?php

namespace App\Http\Controllers;

use App\Models\Worker;
use App\Models\Machine;
use App\Models\WorkHistory;
use Illuminate\Http\Request;


class ApiController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWorkers()
    {
        $workers = Worker::pluck('name');
        return response()->json($workers, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMachines()
    {
        $machines = Machine::pluck('model');
        return response()->json($machines, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function startWork(Request $request)
    {
        $workerId = $request->input('worker_id');
        $machineId = $request->input('machine_id');
        $worker = Worker::find($workerId);
        $machine = Machine::find($machineId);

        if (!$worker && !$machine) {
            return response()->json(['error' => 'Worker and machine do not exist'], 400);
        } elseif (!$worker) {
            return response()->json(['error' => $workerId . ' worker does not exist'], 400);
        } elseif (!$machine) {
            return response()->json(['error' => $machineId . ' machine does not exist'], 400);
        }

        $existingMachine = Machine::where('id', $machineId)->whereNull('worker_id')->first();

        if (!$existingMachine) {
            return response()->json(
                ['error' => $machine->model . ' machine is already occupied by ' . $worker->name], 400
            );
        }

        WorkHistory::create([
            'worker_id' => $workerId,
            'machine_id' => $machineId,
            'started_at' => now(),
            'ended_at' => null,
        ]);

        $existingMachine->worker_id = $workerId;
        $existingMachine->save();

        return response()->json(['message' => $worker->name . ' successfully assigned to machine ' . $machine->model]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function finishWork(Request $request)
    {
        $workerId = $request->input('worker_id');
        $machineId = $request->input('machine_id');
        $worker = Worker::find($workerId);
        $machine = Machine::find($machineId);

        if (!$worker && !$machine) {
            return response()->json(['error' => 'Worker and machine do not exist'], 400);
        } elseif (!$worker) {
            return response()->json(['error' => $workerId . ' worker does not exist'], 400);
        } elseif (!$machine) {
            return response()->json(['error' => $machineId . ' machine does not exist'], 400);
        }

        $existingMachine = Machine::where('id', $machineId)->where('worker_id', $workerId)->first();

        if (!$existingMachine) {
            return response()->json(
                ['error' => $machine->model . ' machine is not assigned to the worker'], 400
            );
        }

        $existingMachine->worker_id = null;
        $existingMachine->save();

        $workHistory = WorkHistory::where('worker_id', $workerId)
            ->where('machine_id', $machineId)
            ->whereNull('ended_at')
            ->first();

        if ($workHistory) {
            $workHistory->update(['ended_at' => now()]);
        }

        return response()->json(
            ['message' => $worker->name . ' successfully finished work on machine ' . $machine->model]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkWorker(Request $request)
    {
        $workerId = $request->input('worker_id');
        $worker = Worker::find($workerId);

        if (!$worker) {
            return response()->json(['error' => 'Worker ' . $workerId . ' does not exist.'], 400);
        }

        $workerMachines = Machine::where('worker_id', $workerId)->get();
        $machineModels = $workerMachines->pluck('model')->toArray();

        if (count($machineModels) > 0) {
            $message = $worker->name . ' is currently working with machines: ' . implode(', ', $machineModels);
        } else {
            $message = $worker->name . ' is not currently assigned to any machines.';
        }

        return response()->json(['message' => $message]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkMachine(Request $request)
    {
        $machineId = $request->input('machine_id');
        $machine = Machine::find($machineId);

        if (!$machine) {
            return response()->json(['error' => 'Machine ' . $machineId . ' does not exist.'], 400);
        }

        if ($machine->worker) {
            $workerName = $machine->worker->name;
            $message = $workerName . ' is currently working with machine ' . $machine->model;
        } else {
            $message = 'No worker is currently assigned to machine ' . $machine->model;
        }

        return response()->json(['message' => $message]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function historyWorker(Request $request)
    {
        $workerId = $request->input('worker_id');
        $worker = Worker::find($workerId);

        if (!$worker) {
            return response()->json(['error' => 'Worker ' . $workerId . ' does not exist.'], 400);
        }

        $history = WorkHistory::where('worker_id', $workerId)
            ->orderBy('started_at', 'asc')
            ->select('machine_id', 'started_at', 'ended_at')
            ->paginate(2);

        return response()->json([
            'worker_name' => $worker->name,
            'history' => $history,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function historyMachine(Request $request)
    {
        $machineId = $request->input('machine_id');
        $machine = Machine::find($machineId);

        if (!$machine) {
            return response()->json(['error' => 'Machine ' . $machineId . ' does not exist.'], 400);
        }

        $history = WorkHistory::where('machine_id', $machineId)
            ->orderBy('started_at', 'asc')
            ->select('worker_id', 'started_at', 'ended_at')
            ->paginate(2);

        return response()->json([
            'machine_model' => $machine->model,
            'history' => $history,
        ]);
    }

}
