# Instructions
1. Run docker-compose up
2. Run projects migration: 
      docker-compose exec app php artisan migrate
3. Run db seeders:
      docker-compose exec app php artisan db:seed --class=WorkerSeeder
      docker-compose exec app php artisan db:seed --class=MachineSeeder
4. Use postman collection for endpoints test (postman_collection.json)